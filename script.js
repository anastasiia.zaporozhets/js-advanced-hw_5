"use strict"

function getUsers() {
    const usersURL = "https://ajax.test-danit.com/api/json/users";

    return fetch(usersURL)
        .then(response => {
            if (!response.ok) {
                throw new Error("Failed to fetch users");
            }
            return response.json();
        })
        .catch(error => {
            throw error;
        });
}

function getPosts() {
    const postsURL = "https://ajax.test-danit.com/api/json/posts";

    return fetch(postsURL)
        .then(response => {
            if (!response.ok) {
                throw new Error("Failed to fetch posts");
            }
            return response.json();
        })
        .catch(error => {
            throw error;
        });
}

class Card {
    constructor(id, name, email, userId, title, body) {
        this.id = id;
        this.userFullName = name;
        this.email = email;
        this.userId = userId;
        this.userTitle = title;
        this.userBody = body;
        this.element = this.createCardElement();
    }

    createCardElement() {
        const div = document.createElement("div");
        div.style.backgroundColor = "black";

        const h4 = document.createElement("h4");
        h4.textContent = ` ${this.userFullName} `;
        const emailSpan = document.createElement("span");
        emailSpan.textContent = `:${this.email}`;
        emailSpan.style.color = "grey";
        h4.appendChild(emailSpan);
        div.appendChild(h4);

        const title = document.createElement("p");
        title.textContent = `${this.userTitle}`;
        title.style.color = "grey";
        const body = document.createElement("p");
        body.textContent = `${this.userBody}`;

        const deleteButton = document.createElement("button");
        deleteButton.textContent = "Видалити пост";
        deleteButton.style.backgroundColor = "grey";
        deleteButton.addEventListener("click", () => {
            this.deleteCard();
        });

        div.append(title);
        div.append(body);
        div.append(deleteButton);

        return div;
    }

    deleteCard() {
        fetch(`https://ajax.test-danit.com/api/json/posts/${this.id}`, {
            method: "DELETE"
        })
            .then(response => {
                if (response.ok) {
                    this.element.remove();
                } else {
                    console.error("Error delete post");
                }
            })
            .catch(error => {
                throw error;
            });
    }
}


function render() {
    Promise.all([getUsers(), getPosts()])
        .then(([users, posts]) => {
            posts.forEach(post => {
                const user = users.find(user => user.id === post.userId);
                if (user) {
                    const card = new Card(post.id, user.name, user.email, user.id, post.title, post.body);
                    document.body.appendChild(card.element);
                }
            });
        })
        .catch(error => {
            throw new Error(error.message);
        });
}

render();
